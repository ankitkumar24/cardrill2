const getLastCar = require("../problem2.cjs");
const inventory = require("../data.cjs");


const car = getLastCar(inventory);
console.log("Last car is a " + car.car_make + " " + car.car_model);
