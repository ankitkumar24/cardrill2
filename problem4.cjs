// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
function getCarYear(inventory) {
  let carArr = []
  if (!Array.isArray(inventory) || inventory.length === 0) {
      return carArr
  }
    const carYears = inventory.map((element) => element.car_year);
    return carYears;
  }
  
  module.exports = getCarYear;
  