// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function getBMWAndAudiCars(inventory) {
  let carArr = []
  if (!Array.isArray(inventory) || inventory.length === 0) {
      return carArr
  }

  const bmwAudiCars = inventory.filter((element) => element.car_make === 'Audi' || element.car_make === 'BMW');
  return bmwAudiCars;
}

module.exports = getBMWAndAudiCars;
