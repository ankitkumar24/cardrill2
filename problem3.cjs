// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortByNames(inventory) {
  let carArr = []
  if (!Array.isArray(inventory) || inventory.length === 0) {
      return carArr
  }
  if(inventory.length===1){
    return inventory;
  }

  const newInventory = [...inventory];

  newInventory.sort((a, b) => {
    const name1 = a.car_make.toUpperCase();
    const name2 = b.car_make.toUpperCase();

    if (name1 < name2) {
      return -1;
    } else if (name1 > name2) {
      return 1;
    } else {
      return 0;
    }
  });

  return newInventory;
}

module.exports = sortByNames;

